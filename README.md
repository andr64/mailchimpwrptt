## Requirements

* Laravel Homestead https://laravel.com/docs/5.6/homestead
* or analog

## Init

* clone code

```
# git clone https://andr64@bitbucket.org/andr64/mailchimpwrptt.git
```

* setup http server to serve `public` folder as wwwroot
* rename file `.env.example` to `.env`
* open `.env` file, and fill MailChimp and DB credentials

```
MAILCHIMP_BASE=
MAILCHIMP_KEY=
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

* dependencies

```
# composer install
```

* init DB

```
# php artisan migrate:fresh
```

* ready to use
* all implementation in `roures/api.php`

## Usage

* set bash env `# API=http://localhost/api/`
* send all HTTP request to `$API`
* any HTTP METHOD
* returns in json format
* in error case will returns `{"error": <error>}`


### create list

```
# curl $API/list_new -d 'name=<NewListName>'

returns created list object
{
  "id": "xxx",
  "name": "NewListName",
  ...
}
```


### update list

```
# curl $API/list_upd -d 'id=<TargetListId>' -d 'name=<NewName>'

returns updated list object
{
  "id": "xxx",
  "name": "NewName",
  ...
}
```


### delete list

```
# curl $API/list_del -d 'id=<TargetListId>'

returns null
```


### view lists

```
# curl $API/lists -d 'local=<0|1(default)>'
local=0 - from local sotrage
local=1 - from MailChimp API

returns {
  "lists": [<array of lists objects>]
}
```


### create member

```
# curl $API/member_new -d 'list=<TargetListId>' -d 'email=<Member@Email.Address>'

returns created member object
{
  "id": "85c32f...",
  "email_address": "Member@Email.Address",
  ...
}
```


### update member

```
# curl $API/member_upd -d 'list=<TargetListId>' -d 'id=<TargetMemberId>' -d 'email=<New@Email.Address>'

returns updated member object
{
  "id": "171a57...",
  "email_address": "New@Email.Address",
  ...
}
```


### delete member

```
# curl $API/member_del -d 'list=<TargetListId>' -d 'id=<TargetMemberId>'

returns null
```


### members lists

```
# curl $API/members -d 'list=<TargetListId>' -d 'local=<0|1(default)>'
local=0 - from local sotrage
local=1 - from MailChimp API

returns {
  "members": [<array of members objects>]
}
```