<?php

use GuzzleHttp\Client;  # http://guzzle.readthedocs.io/en/latest/quickstart.html
use Illuminate\Http\Request;
use App\Lists;
use App\Members;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


# credentials

define('MAILCHIMP_BASE', 'https://us12.api.mailchimp.com/3.0/');
define('MAILCHIMP_KEY', 'xxx-us12');


# helpers

function x_mc_req ($method, $cmd, $params) {
  # curl --request GET --url 'https://us12.api.mailchimp.com/3.0/lists' --user 'anystring:xxx-us12'
  $cli = new Client();
  $params['auth'] = ['anystring', MAILCHIMP_KEY];
  $params['http_errors'] = false;
  $res = $cli->request($method, MAILCHIMP_BASE.$cmd, $params);
  return $res;
}

function x_resp ($data) {
  $response = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
  return response($response)->header('Content-Type', 'application/json');
};


# todo: move all endpoints to Controllers
# todo: use auth:api middleware


# list creator

Route::get('list_new', function (Request $request) {
  
  # http://developer.mailchimp.com/documentation/mailchimp/reference/lists/#create-post_lists
  
  # create list instance via mailchimp api
  # todo: check request results
  $res = x_mc_req('POST', 'lists', [
    'headers' => ['content-type' => 'application/json'],
    'body' => json_encode([
      'name' => $request->input('name', 'UnnamedList at '.date('YmdHis')),
      # and other fields hardcoded
      'permission_reminder' => 'News for You',
      'email_type_option' => true,
      'contact' => ['company' => 'MyCompany',  'address1' => 'Addr1', 'city' => 'City1', 'state' => 'State1', 'zip' => '98765', 'country' => 'ZZ'],
      'campaign_defaults' => ['from_name' => 'admin', 'from_email' => 'admin@mycompany.com', 'subject' => '', 'language' => 'en']
    ])
  ]);
  $body = $res->getBody();
  
  # create local instance
  $l = new Lists;
  $l->data = $body;  # type of 'data' field is 'json'
  # todo: use dedicated field for mailchimp's version id with index
  $l->save();
  
  # ... or native sql?
  # DB::insert('insert into lists (data) values (?)', [$body]);
  
  # done
  return x_resp(json_decode($body));
  
});



Route::get('list_upd', function (Request $request) {
  
  # http://developer.mailchimp.com/documentation/mailchimp/reference/lists/#edit-patch_lists_list_id
  
  $id = $request->input('id', '');
  if ($id == '') return json_encode(['error' => 'id required']);
  
  # update list instance via mailchimp api
  
  $patch = [];
  $p_name = $request->input('name');
  if (!is_null($p_name)) $patch['name'] = $p_name;
  # todo: more fields for update ...
  
  if (count($patch) < 1) return json_encode(['error' => 'nothing to change']);
  
  # update mailchimp instance
  # ! todo: check request results
  $res = x_mc_req('PATCH', 'lists/'.$id, [
    'headers' => ['content-type' => 'application/json'],
    'body' => json_encode($patch)
  ]);
  $body = $res->getBody();
  
  # update local instance
  # todo: use dedicated field for mailchimp's version id with index
  DB::select('update lists set data = ? where data->"$.id" = ? limit 1', [$body, $id]);
  
  # done
  return x_resp(json_decode($body));
  
});


# lists deleter

Route::get('list_del', function (Request $request) {
  
  # http://developer.mailchimp.com/documentation/mailchimp/reference/lists/#delete-delete_lists_list_id
  
  $id = $request->input('id', '');
  
  if ($id == '') return json_encode(['error' => 'id required']);
  
  $res = x_mc_req('DELETE', 'lists/'.$id, []);
  $answ = json_decode($res->getBody());
  
  # in success case returns: null
  # in fail case returns: {"error": <mailchimperror>}
  
  if (is_null($answ)) {
    
    # successfuly deleted from mailchimp
    
    # delete local instance
    # todo: use dedicated field for mailchimp's version id with index
    DB::delete('delete from lists where data->"$.id" = ?', [$id]);
    
    return x_resp(null);
   
  } else {
    
    # mailchipm error
    return x_resp(['error' => json_decode($res->getBody())]);
   
  }
  
});



# lists viewer

Route::get('lists', function (Request $request) {
  
  # http://developer.mailchimp.com/documentation/mailchimp/reference/lists/#read-get_lists
  
  if ($request->input('local', '1') == '0') {
    
    # todo: pagenation?
    $res = x_mc_req('GET', 'lists', []);
    return x_resp(json_decode($res->getBody()));
    
  } else {
    
    # model based version
    $lists = App\Lists::all();
    $ll = array();
    foreach ($lists as $l) $ll[] = json_decode($l->data);
    return x_resp(['lists' => $ll]);
    
  }
  
});




# member ceator

Route::get('member_new', function (Request $request) {
  
  # http://developer.mailchimp.com/documentation/mailchimp/reference/lists/members/#create-post_lists_list_id_members

  $list = $request->input('list', '');
  $email = $request->input('email', '');
  if ($list == '') return json_encode(['error' => 'list required']);
  if ($email == '') return json_encode(['error' => 'email required']);
  
  # todo: check list exists
  
  # create instance via mailchimp api
  # todo: check request results
  $res = x_mc_req('POST', 'lists/'.$list.'/members', [
    'headers' => ['content-type' => 'application/json'],
    'body' => json_encode([
      'email_address' => $email,
      'status' => 'subscribed'  # temporary hardcoded
    ])
  ]);
  $body = $res->getBody();
  
  # todo: validate $body
  $j_body = json_decode($body, true);
  
  if (isset($j_body['id'])) {
    
    # create local instance
    # native sql
    # todo: create dedicated fields: "list_id"
    DB::insert('insert into members (data) values (?)', [$body]);
    
    # done
    return x_resp($j_body);
    
  } else {
    
    # mailchipm error
    return x_resp(['error' => $j_body]);
    
  }
  
});



# member update

Route::get('member_upd', function (Request $request) {
  
  # http://developer.mailchimp.com/documentation/mailchimp/reference/lists/members/#edit-patch_lists_list_id_members_subscriber_hash
    
  $id = $request->input('id', '');
  $list = $request->input('list', '');
  if ($id == '') return json_encode(['error' => 'id required']);
  if ($list == '') return json_encode(['error' => 'list required']);
  
  # prepare update params
  $patch = [];
  $p_email = $request->input('email');
  if (!is_null($p_email)) $patch['email_address'] = $p_email;
  # todo: more fields for update ...
  
  if (count($patch) < 1) return json_encode(['error' => 'nothing to change']);
  
  # update mailchip instance
  # todo: check request results
  $res = x_mc_req('PATCH', 'lists/'.$list.'/members/'.$id, [
    'headers' => ['content-type' => 'application/json'],
    'body' => json_encode($patch)
  ]);
  $body = $res->getBody();
  
  # update local instance
  # todo: use dedicated field for mailchimp's version id with index
  DB::select('update members set data = ? where data->"$.list_id" = ? and data->"$.id" = ? limit 1', [$body, $list, $id]);
  
  # done
  return x_resp(json_decode($body));
  
});





# members deleter

Route::get('member_del', function (Request $request) {
  
  # http://developer.mailchimp.com/documentation/mailchimp/reference/lists/members/#delete-delete_lists_list_id_members_subscriber_hash
  
  $list = $request->input('list', '');
  $id = $request->input('id', '');
  if ($list == '') return json_encode(['error' => 'list required']);
  if ($id == '') return json_encode(['error' => 'id required']);
  
  $res = x_mc_req('DELETE', 'lists/'.$list.'/members/'.$id, []);
  $answ = json_decode($res->getBody());
  
  # success returns: null (HTTP/1.1 204 No Content)
  # fail returns: {"error": <mailchimperror>}
  
  if (is_null($answ)) {
    
    # successfuly deleted from mailchimp
    
    # delete from local DB
    # todo: use dedicated indexed "id" field
    DB::delete('delete from members where data->"$.list_id" = ? and data->"$.id" = ?', [$list, $id]);
    
    return x_resp(null);
    
  } else {
    
    # mailchipm error
    return x_resp(['error' => json_decode($res->getBody())]);
    
  }
  
});


# members viewer

Route::get('members', function (Request $request) {
  
  # http://developer.mailchimp.com/documentation/mailchimp/reference/lists/members/#read-get_lists_list_id_members
   
  $list = $request->input('list', '');
  if ($list == '') return json_encode(['error' => 'list required']);
 
  if ($request->input('local', '1') == '0') {
    
    # todo: pagenation?
    $res = x_mc_req('GET', 'lists/'.$list.'/members', []);
    return x_resp(json_decode($res->getBody()));
    
  } else {
    
    # native sql version
    # todo: use dedicated indexed "list_id" field
    $members = DB::select('select * from members where data->"$.list_id" = ?', [$list]);
    
    $ll = array();
    foreach ($members as $l) $ll[] = json_decode($l->data);
    return x_resp(['members' => $ll]);
    
  }
  
});


#
# todo: lists/members sync
#


# appendix
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/